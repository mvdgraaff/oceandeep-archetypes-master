#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test Class.
 */
public class ${artifactId}Test {

 /**
  * Set up any state required prior to running the tests.
  * 
  * @throws Exception
  *      when something goes wrong
  */
 @Before
 public final void setUp() throws Exception {
  // TODO implement the pre-test set up
 }

 /**
  * Clean up anything required after running the tests.
  * 
  * @throws Exception
  *      when something goes wrong
  */
 @After
 public final void tearDown() throws Exception {
  // TODO implement the post-test tear down
 }

 /**
  * Still needs to be implemented.
  */
 @Test
 public final void test1() {
  // TODO implement the tests for doService()
  assertTrue(false);
 }
}
