#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright information goes here
 */
package ${package};

import com.sonicsw.esb.service.common.SFCParameters;
import com.sonicsw.esb.service.common.SFCServiceContext;
import com.sonicsw.esb.service.common.impl.AbstractSFCServiceImpl;
import com.sonicsw.xq.XQEnvelope;
import com.sonicsw.xq.XQMessage;
import com.sonicsw.xq.XQServiceException;
import org.apache.log4j.Logger;

/**
 * ${artifactId} Description.
 */
public class ${artifactId} extends AbstractSFCServiceImpl {
 // Access to the SFC's logging mechanism
 private final Logger log_     = Logger.getLogger(this.getClass());

 /**
  * Process each incoming message.
  * 
  * @param _ctx
  *     runtime context of processing
  * @param _envelope
  *     contains the incoming message
  * @throws XQServiceException if the message cannot be correctly processed - message will be set to RME
  * @see com.sonicsw.esb.service.common.impl.AbstractSFCServiceImpl${symbol_pound}doService(
  *   com.sonicsw.esb.service.common.SFCServiceContext, com.sonicsw.xq.XQEnvelope)
  */
 public final void doService(final SFCServiceContext _ctx, final XQEnvelope _envelope) throws XQServiceException {

  // get the parameters from the Service Context
  final SFCParameters parameters = _ctx.getParameters();

  // get the message from the envelope
  final XQMessage message = _envelope.getMessage();
  
  log_.info("Processing incoming message");

  // Custom Service Implementation Goes Here...
  
  log_.info("Passing the message to the outbox.");
  _ctx.addIncomingToOutbox();
 }
}
